import React, { Component, Fragment } from "react";
import styled from "styled-components";
import defuser from "../Image/defuser.jpg";
import bread from "../Image/bread.jpg"
import coffee from "../Image/coffee.jpg"
import Hanwoo from "../Image/Hanwoo.png"
import Card from "../Components/Card/Card"
import Card2 from "../Components/Card/Card2"
import PropTypes from "prop-types";


const ItemUI = styled.ul`
  

`

const ItemLi = styled.li`
  display: flex; 
  justify-content: space-between;
  align-items:flex-start;
  flex-wrap:wrap;
  margin-bottom : 20px;
`


class Item extends Component {

    constructor(props) {
        super(props);

        this.state = {
            items: [
                {
                    id: 1,
                    poster: defuser,
                    cardLabel: "Card Label 1",
                    cardTitle: "Card Title 1",
                    hilight: "Hilight 1",
                    corossOut: "Coross Out 1",
                    star: 5,
                    comment: "Lorem ipsum dolor sit ametconsectur adipising elit",
                },
                {
                    id: 2,
                    poster: bread,
                    cardLabel: "Card Label 2",
                    cardTitle: "Card Title 2",
                    hilight: "Hilight 2",
                    corossOut: "Coross Out 2",
                    star: 4,
                    comment: "",

                },
                {
                    id: 3,
                    poster: coffee,
                    cardLabel: "Card Label 3",
                    cardTitle: "Card Title 3",
                    hilight: "Hilight 3",
                    corossOut: "Coross Out 3",
                    star: null,
                    comment: "",

                },
                {
                    id: 4,
                    poster: Hanwoo,
                    cardLabel: "Card Label 4",
                    cardTitle: "Card Title 4",
                    hilight: "Hilight 4",
                    corossOut: "Coross Out 4",
                    star: 2,
                    comment: "Lorem ipsum dolor sit amet consectur adipising elit first and for most",

                }
            ]
        };
    }


    _renderItems = () => {
        const items = this.state.items.map(item => {
            return (
                <Card
                    key={item.id}
                    poster={item.poster}
                    cardLabel={item.cardLabel}
                    cardTitle={item.cardTitle}
                    hilight={item.hilight}
                    corossOut={item.corossOut}
                    star={item.star}
                    comment={item.comment}
                />
            )
        }
        )
        return items;
    };

    _horizonrenderItems = () => {
        const items = this.state.items.map(item => {
            return (
                <Card2
                    key={item.id}
                    poster={item.poster}
                    cardLabel={item.cardLabel}
                    cardTitle={item.cardTitle}
                    hilight={item.hilight}
                    corossOut={item.corossOut}
                    star={item.star}
                    comment={item.comment}
                />
            )
        }
        )
        return items;
    };





    render() {
        const { items } = this.state;
        return (
            <Fragment >
                <ItemUI>
                    <ItemLi>
                        {items ? this._renderItems() : "loading"}
                    </ItemLi>
                    <ItemLi>
                        {items ? this._horizonrenderItems() : "loading"}
                    </ItemLi>
                </ItemUI>
            </Fragment >
        );

    }
}

Item.propTypes = {
    poster: PropTypes.any.isRequired,
    cardLabel: PropTypes.string.isRequired,
    cardTitle: PropTypes.string.isRequired,
    hilight: PropTypes.string.isRequired,
    corossOut: PropTypes.string.isRequired,
    star: PropTypes.number,
    comment: PropTypes.string.isRequired
  }
  
export default Item;

