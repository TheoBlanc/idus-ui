import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import LinesEllipsis from "react-lines-ellipsis"
import Rating from "react-rating";
import { FullStar, EmptyStar } from "../../Image/icon"


const CardWrap = styled.div`
  flex-basis: 20%;

`

const Card = styled.div`
  box-sizing:border-box;
  ${props => props.theme.whiteBox};

`;

const CardImageWrap = styled.div`

`

const Poster = styled.div`
  max-width: 100%;
  width: 209px;
  height: 209px;
  background-image: url(${props => props.src});
  background-size: cover;
  background-position: center;
`
const Link = styled.a`

`


const CardInfo = styled.div`
    height: 96px;
    padding: 8px 10px;
`

const CardLabel = styled.a`
    color: #999;
    display: block;
    font-size: 12px;
    height: 15px;
    margin-bottom: 4px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 100%;
`
const CardTitle = styled.a`
    color: ${props => props.theme.blackColor};
    display: block;
    font-size: 14px;
    height: 42px;
    line-height: 1.5;
    overflow: hidden;
    text-overflow: ellipsis;
`

const Hilight = styled.span`
    color: red;
    font-size: 12px;


`

const CorossOut = styled.span`
    margin-left : 5px;
    color: gray;
    font-size: 10px;
    text-decoration: line-through ;
`


const RatingWrap = styled.div`
    display: table;
    border-top: 1px solid #d9d9d9;
    box-sizing: border-box;
    width: 100%;

`

const CardRating = styled.div`
    display: table-cell;
    vertical-align: middle;
    padding: 0 10px;
    height: 56px;
    width: 100%;

`
const StarRating = styled.div`
    font-size: 12px;
    padding: 6px 0 4px;
`
const Comment = styled.div`
    font-size: 12px;
    height: 18px;
    line-height: 1.5;
`

const ItemCard = ({ poster, cardLabel, cardTitle, hilight, corossOut, star, comment }) => (
  <CardWrap>
    <Card>
      <CardImageWrap>
        <Link href="#">
          {poster && <Poster src={poster} />}
        </Link>
      </CardImageWrap>

      <CardInfo>
        <CardLabel href="#">{cardLabel}</CardLabel>
        <CardTitle href="#">{cardTitle}</CardTitle>
        <Hilight>{hilight}</Hilight>
        <CorossOut>{corossOut}</CorossOut>
      </CardInfo>


      {star && (

        <RatingWrap>
          <CardRating>
            <StarRating>
              <Rating emptySymbol={<EmptyStar />} fullSymbol={<FullStar />} initialRating={star} readonly />

            </StarRating>
            {comment && (

              <Comment>
                <LinesEllipsis
                  text={comment}
                  maxLine='1'
                  ellipsis='...'
                  trimRight
                  basedOn='letters'
                />
              </Comment>


            )}


          </CardRating>
        </RatingWrap>
      )}



    </Card>
  </CardWrap>


);

ItemCard.propTypes = {
  poster: PropTypes.any.isRequired,
  cardLabel: PropTypes.string.isRequired,
  cardTitle: PropTypes.string.isRequired,
  hilight: PropTypes.string.isRequired,
  corossOut: PropTypes.string.isRequired,
  star: PropTypes.number,
  comment: PropTypes.string.isRequired
}


export default ItemCard;



