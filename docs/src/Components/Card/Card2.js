import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Rating  from "react-rating";
import {FullStar, EmptyStar} from "../../Image/icon"



const Container = styled.div`
    margin-bottom: 20px;
    display: block;
    width: 46.5%;
    box-sizing:border-box;
    ${props => props.theme.whiteBox};
`
const Containerinner = styled.div`
    display: table;
    position: relative;
    width: 100%;
    box-sizing: border-box;
    text-align: left;

`

const CardImageWrap = styled.div`
    display: table-cell;
    width: 209px;

`

const Poster = styled.div`
  max-width: 100%;
  width: 100%;
  height: 209px;
  background-image: url(${props => props.src});
  background-size: cover;
  background-position: center;
  `
const Link = styled.a``;

const Textarea = styled.div`
    display: table-cell;
    vertical-align: top; 
    height: auto;
    padding: 0 20px;   
`

const CardInfo = styled.div`
    height: auto;
`
const CardTitle = styled.a`
    display: block;
    color: ${props => props.theme.blackColor};
    font-size: 14px;
    font-weight: 700;
    height: 21px;
    line-height: 1.5;
    margin: 15px 0;
    overflow: hidden;
    text-overflow: ellipsis;
`

const Comment = styled.div`
    font-size: 14px;
    height: 105px;
    line-height: 1.5;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: pre-line;
`
const CardRating = styled.div`
  align-items: center;
  display: flex;

`

const StarRating = styled.span`
    font-size: 12px;
    padding: 6px 0 4px;
`

const CardLabel = styled.a`
    color: #999;
    font-size: 14px;
    text-overflow: ellipsis;
`

const HorizonCard = ({ poster, cardLabel, cardTitle, star, comment }) => (
    <Container>
        <Containerinner>
        <CardImageWrap>
        <Link href="#">
          {poster && <Poster src={poster} />}
          
        </Link>
      </CardImageWrap>

      <Textarea>
          <CardInfo>
          <CardTitle href="#">{cardTitle}</CardTitle>
          <Comment>{comment}</Comment>



          </CardInfo>
          <CardRating>
          <StarRating><Rating emptySymbol={<EmptyStar/>} fullSymbol={<FullStar/>} initialRating={star} readonly/></StarRating>
          
          <CardLabel href="#"> &nbsp; | {cardLabel}</CardLabel>

          </CardRating>
          
      </Textarea>
            
        </Containerinner>

    </Container>


);

HorizonCard.propTypes = {
  poster: PropTypes.any.isRequired,
  cardLabel: PropTypes.string.isRequired,
  cardTitle: PropTypes.string.isRequired,
  hilight: PropTypes.string.isRequired,
  corossOut: PropTypes.string.isRequired,
  star: PropTypes.number,
  comment: PropTypes.string.isRequired
}


export default HorizonCard;


