import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";


const Container = styled.input`
    width: 600px;
    height: 100px;
`;

const Input = ({
  placeholder,
  value,
  onChange,
  type,
  className,
  maxLength,
  disabled,
  readOnly,
  onFocus,
  onBlur
}) => (
    <Container
      className={className}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      type={type}
      maxLength={maxLength}
      disabled={disabled}
      readOnly={readOnly}
      onFocus={onFocus}
      onBlur={onBlur}
    />
  );

Input.propTypes = {
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  maxLength: PropTypes.string,
  disabled: PropTypes.string,
  readOnly: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func
};

export default Input;