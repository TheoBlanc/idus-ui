import React from "react";
import GlobalStyles from "../Styles/GlobalStyles";
import styled, { ThemeProvider } from "styled-components";
import Theme from "../Styles/Theme";
import Item from "../Item/ItemPresenter";
import InputA from "./Input/InputA";
import InputB from "./Input/InputB";
import InputC from "./Input/InputC";

const Wrap = styled.div`
    width: 1056px;
    margin: 50px auto;
    position: relative;
`


export default () => {

  return (
    <ThemeProvider theme={Theme}>
      <>
        <GlobalStyles />
        <Wrap>
          <Item />
          <InputA />
          <InputA />
          <InputB />
          <InputC />

        </Wrap>

      </>

    </ThemeProvider>
  );
}
