import React, { useState } from 'react';
import styled from "styled-components";
import Input from "../input"


const Wrap = styled.ul`
    position:relative;
    margin-bottom:10px;
`
const WrapInner = styled.li`
`

const StInput = styled(Input)`

`

const Count = styled.div`
    position: absolute;
    top:80px;
    left:570px;
    color:gray;
    font-size:12px;
`

const Letter = () => {
    const [value, setValue] = useState('');
    const [toggle, setToggle] = useState(false);

    const onChangeValue = e => {
        setValue(e.target.value);
    };

    const check = (str, len) => {
        if (len - str.length === 0) {
            alert("초과하였습니다.")
        }
        return len - str.length;
    }

    const Button = styled.button`
    background: ${props => props.primary ? "palevioletred" : "white"};
    color: ${props => props.primary ? "white" : "palevioletred"};
    font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
  `;

  const onFocusUpValue = () => {
    setToggle(!toggle)

  }
  const onBlur = () => {
    setToggle(!toggle)

  }





    return (
        <>
            <Wrap>
                <WrapInner>
                    <form>
                        <label>
                            <StInput className="value" type="text" value={value} onChange={onChangeValue}
                                maxLength='10' placeholder="초기값이 있을수 있습니다." onFocus={onFocusUpValue}
                                onBlur={onBlur}
                                />
                            <Count>{check(value, 10)}</Count>
                            {toggle ? (<Button primary>Save</Button>) : (<Button >Save</Button>)}
                            
                            

                        </label>
                    </form>
                    
                </WrapInner>


            </Wrap>



        </>

    );

}

export default Letter;