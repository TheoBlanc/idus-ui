import React, { useState } from 'react';
import styled from "styled-components";
import Input from "../input"


const Wrap = styled.ul`
    position:relative;
    margin-bottom:10px;
`
const WrapInner = styled.li`
`

const StInput = styled(Input)`

`

const Count = styled.div`
    position: absolute;
    top:80px;
    left:570px;
    color:gray;
    font-size:12px;
`



const Letter = () => {
    const [value, setValue] = useState('');

    const onChangeValue = e => {
        setValue(e.target.value);
    };

    const check = (str, len) => {
        if (len - str.length === 0) {
            alert("초과하였습니다.")
        }
        return len - str.length;
    }

    return (
        <>
            <Wrap>
                <WrapInner>
                    <form>
                        <label>
                            <StInput className="value" type="text" value={value} onChange={onChangeValue}
                                maxLength='10' placeholder="주문 요청사항을 입력해주세요."
                                disabled
                                />
                            <Count>{check(value, 10)}</Count>
                            
                            

                        </label>
                    </form>
                    
                </WrapInner>


            </Wrap>



        </>

    );

}

export default Letter;