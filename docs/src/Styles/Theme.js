const BOX_BORDER =  "1px solid #d9d9d9"
const BORER_RADIUS = "4px"
const BGCOLOR = "#f8f9fb"

export default {
    blackColor: "#333",
    
    whiteBox : `border:${BOX_BORDER};
                border-radius:${BORER_RADIUS};
                background-color:${BGCOLOR}
               `


}